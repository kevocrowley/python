#!/usr/bin/python3
'''
So I am writing this script to check for IP addresses and whether they are responding the IP pings
'''

from subprocess import PIPE, run

hostname=input("Hello my friend please input an IP address and I will tell you if it is online or not")
command = ['ping', '-c', '1', hostname]
result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
ipoutput = result.stdout

for line in ipoutput.split(", "):
	if line == '0% packet loss':
		print("Yes no packet loss for this one")
	elif line == '100% packet loss':
		print("Man this IP address is totally bogus")