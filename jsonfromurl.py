#Another JSON example
#!/usr/bin/python3
import json
from urllib.request import urlopen	

with urlopen("https://jsonplaceholder.typicode.com/users") as urlrequest:
	source = urlrequest.read()

output_file=[]
#So you can now print out the string of the JSON data returned from the API
#This will not be in a friendly format to read for humans so lets....make it more friendly
new_datar=json.loads(source)
for n in range(0,len(new_datar)):
	output_file.append(new_datar[n]["username"])
	output_file.append(new_datar[n]["email"])
	#output_file.append(new_datar[n]["company"])

print(output_file)

with open("kevin_json_file.txt","w") as kjson:
	kjson.write('\n'.join(output_file))


'''
for n in range(0,5):
	print(json.dumps(new_datar[n]["name"]))
	print(json.dumps(new_datar[n]["username"]))
'''
