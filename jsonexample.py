
import json

kevin_string =''' 
{
    "firstName": "Jane",
    "lastName": "Doe",
    "hobbies": ["running", "sky diving", "singing"],
    "age": 35,
    "children": [
        {
            "firstName": "Alice",
            "age": 6
        },
        {
            "firstName": "Bob",
            "age": 8
        }
    ]
}
'''

data = json.loads(kevin_string)
if data["firstName"] =='Jane':
	print("yes")

for data_new in data['firstName']:
	print(data_new)

#print(data)

#new_data = json.dumps(data, indent=2)
#print(new_data)
