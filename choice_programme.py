#!/usr/bin/python3
'''
This is going to be used to drive the choice of program for a user
They will have a bit more friendly interactive programming with this rather than iterating through the whole scipt
'''
from __future__ import print_function, unicode_literals

from PyInquirer import style_from_dict, Token, prompt, Separator
from pprint import pprint


style = style_from_dict({
    Token.Separator: '#cc5454',
    Token.QuestionMark: '#673ab7 bold',
    Token.Selected: '#cc5454',  # default
    Token.Pointer: '#673ab7 bold',
    Token.Instruction: '',  # default
    Token.Answer: '#f44336 bold',
    Token.Question: '',
})

questions = [
    {
        'type': 'checkbox',
        'message': 'Select Game',
        'name': 'Choose',
        'choices': [
            Separator('= The Games ='),
            {
                'name': 'Ping Check'
            },
            {
                'name': 'Gaa'
            },
        ],
        'validate': lambda answer: 'You must choose at least one game.' \
            if len(answer) == 0 else True
    }
]

def main():
	answers = prompt(questions, style=style)
	#stringer = ""
	#stringy = stringer.join(answers['Choose'])
	stringy = ''.join(answers['Choose'])
	print(stringy)
	if stringy == "Gaa":
		print("HAHA you bad mother fer")
	else:
		print("I don't know what you are")
	print(type(answers['Choose']))


if __name__=='__main__':
	main()