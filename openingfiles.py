'''
This is going to go throug reading and writing from files
'''
'''
with open('darkness.txt','r') as f:
	f_contents = f.read(1000)
	print(f_contents)
	for n in f_contents:
			print(n)
'''	


'''
count=0
with open('darkness.txt','r') as f:
	for line in f:
		count += count+1
		print(line, end='')
print(count)
'''

with open('darkness.txt','r') as f:
	size_to_read=100
	f_contents=f.read(size_to_read)
	while len(f_contents) > 0:
		print(f_contents, end='')
		f_contents=f.read(size_to_read)

with open('kevinfilenew2.txt','w') as f:
	f.write("You are a bad man")
	f.seek(0)
	f.write("Hello again my good man")

with open('darkness.txt', 'r') as rf:
	with open('newdarkness.txt','w') as wf:
		for line in rf:
			wf.write(line)

'''
In order to read binary files such as picture file
do the followig, just add b for binary into the edit mode of the file
'''
with open('picture.png', 'rb') as rf:
	with open('newdarkness.png','wb') as wf:
		for line in rf:
			wf.write(line)

with open('picture.png', 'rb') as rf:
	with open('newdarkness.png','wb') as wf:
		chunk_size = 4096
		rf_chunk = rf.read(chunk_size)

		while len(rf_chunk) > 0:
			wf.write(rf_chunk)
			rf_chunk = rf.read(chunk_size)