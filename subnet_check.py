#!/usr/bin/python3
import os
from subprocess import PIPE, run
import textwrap
'''
class SubnetCheck():
	def __init__(self,dnsname,ipaddress):
		self.dnsname=dnsname
		self.ipaddress=ipaddress

	def dnscheck(hasname=True, domain='na'):
		self.hasname=hasname
		self.domain=domain
'''

def ping_check(hostname):
	command = ['ping', '-c', '1', hostname]
	result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
	ipoutput = result.stdout
	for line in ipoutput.split(", "):
		if line == '0% packet loss':
			print("Yes no packet loss for this one")
		elif line == '100% packet loss':
			print("Man this IP address {} is totally bogus".format(hostname))
	main()

def read_fromfile(filename):
	with open(filename,'r') as ipfile:
		ip_addr=ipfile.readline()
		print(ip_addr)

def inputtype():
	answer=input("How do you want to check for IP address? Manually enter 'm' or from a file press 'f'")
	while True:
		try:
			if answer is 'f':
				print("Please enter the name of the file, please copy file to same location as script")
				ansfile=input("Filename: ")
				read_fromfile(ansfile)

			elif answer is 'm':
				newanswer=input("Please enter the IP address in hex format X.X.X.X")
				ping_check(newanswer)
				break
			elif answer == 'f':
				read_fromfile()
		except:
			print("Please enter a valid character")



def ipaddressinput(ip_addr):
	ip_addr=input('Please enter in your IP address')

def main():
	print("I am the main function driving the check, I will call all other functions"'\n')
	inputtype()

def get_banner():
    return textwrap.dedent("""\
    Hello everyone, this is my interface
               _____     
        | /   |
        |/    | 
        |\    |
        | \   _|____
        KEVIN CROWLEY     
    \x00""")
print(get_banner())
if __name__ == 'main':
	main()
else:
	main()

