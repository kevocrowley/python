import os
import smtplib
import requests

EMAIL_ADDRESS = os.environ.get('EMAIL_USER')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASS')

r = requests.get('https://coreyms.com', timeout=5)

if r.status_code != 200:
	with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
		smtp.ehlo()
		smtp.starttls()
		smtp.ehlo()
		smtp.login(EMAIL_ADDRESS,EMAIL_PASSWORD)

		subject = 'Your site is down at the moment'
		body = 'Make sure that the server has been restarted and is now back up'
		msg = f'Subject: {subject}\n\n{body}'

		smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, msg)


