#!/usr/bin/python3

'''
this is my example of how to return an item from a dictionary list
'''
import heynow

def return_list_item(argument):
	my_dict={1:"pencil",2:"biro",3:"marker"}
	return my_dict.get(argument,"You should have picked something here")

if __name__=='__main__':
	print(return_list_item(2))
	print(return_list_item(4))
	print("From here I am going to run the heynow function reg_check")
	print(heynow.reg_check())




