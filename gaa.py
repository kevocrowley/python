#!/usr/bin/python3

'''
So I've created a new branch that I want to test here..
Just going to mess around with data structures here to help with my learning
'''

cork=['connolly','maguire','mccarthy','odriscoll']
mayo=['oshea','barrett','parsons','boyle','higgins','oconnor']
kerry=['clifford','moran','murphy','oshea','oconnor']
dublin=['cluxton','cooper','fenton','kilkenny','ocallaghan']
leap=('juliette','brodie','michelle','kevin')

if 'cluxton' in dublin:
	print("Cluxton the boy is indeed there")

for names in mayo:
	print(names)

cork.insert(0,'tompkins')
dublin.append('connolly')
dublin[0]='millvinilli'
print(cork)
print(dublin)

cork.extend(dublin)
print(cork)

cork.remove('kilkenny')
print(cork)
print(len(cork))
cork.pop()
cork.pop(0)
print(cork)
#convert to a strin
','.join(kerry)
print(type(kerry))
print(kerry)
