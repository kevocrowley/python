#!/usr/bin/python3

'''
This is just a container for messing with data structures.
'''

dict={'Mercury':88,'Venus':224,'Earth':365,'Mars':687,'Jupiter':11.86,'Saturn':29,'Uranus':84,'Neptune':164}
list=['5000000','This is a string sentence',45,'Jupiter']
tuple=('Jupiter','Saturn','Mercury','Venus','Mars','Earth','Neptune','Uranus')

dict['Earth']=365.25
dict['Pluto']=500
del dict['Earth']

length=len(tuple)
#list.append('Betelguese')
#for item in range(length):
#	print(tuple[item])
for item in list:
	print(item)

for item in tuple:
	list.append(item)
	print(item)

#this will print out the key value for each dictionary item, notice the difference from the below.
for item in dict:
	print(dict[item])	

#This will print out the dictionary key 
for item in dict:
	print(item)

#print(list)

list.insert(0,'Langer')
list.pop(4)
print(list)