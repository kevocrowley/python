#!/usr/bin/python3

'''
This is an awesome piece of coding. The for loops work perfectly and need a bit of thinking about to see why this works this way
I wish i came up with this piece. A lot of iteration through the list but it works very well. 
'''

arr = [64, 1234, 20, 8, 88, 243554,9000000,34,45,43,5234,5234,24523,6,7,3753,5473,5473,54,0]
print(arr)
def bubbleSort(arr):
	# n is now equal to number of items in list
    n = len(arr)
 
    # Traverse through all array elements
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                print(arr)

    return(arr)

print('\n')
print(bubbleSort(arr))
