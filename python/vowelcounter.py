#!/usr/bin/python3

#Vowel counter
arr=['a','e','i','o','u']

def count_letter(answer):
	count = 0
	vowel='vowels'
	print("You entered: {}".format(answer))
	for n in answer:
		if n in arr:
			count += 1
	if count == 1:
		vowel='vowel'
	print("{} has {} {} in it".format(answer,count,vowel))


def main():
	print("Hello user, I will count a number of vowels in your words")
	answer=input("Please enter a word of your choice:")
	while True:
		try:
			if len(answer) > 0
				count_letter(answer)
				break
			else:
				main()
		except:
			print("Not a word")


if __name__ == '__main__':
	main()