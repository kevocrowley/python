#!/usr/bin/python3
'''
So this is my attempt at providing some kind of command line options to the user of the program.
I want to point this to other programs and use this as the interface to call them with
''' 
import textwrap

def list_of_options(argument): 
    switcher = { 
        1: "ping", 
        2: "reg_ex", 
        3: "Checkdisk", 
    } 
    print(switcher)
    z=len(switcher)
    for n in range(1,z):
        print(switcher[n])
    #while True:
     #   print("Please enter the program you want to access")
      #  if 
    # get() method of dictionary data type returns  
    # value of passed argument if it is present  
    # in dictionary otherwise second argument will 
    # be assigned as default value of passed argument 
    return switcher.get(argument, "nothing") 

def get_banner():
    return textwrap.dedent("""\
    Hello everyone, this is my interface
               _____     
        | /   |
        |/    | 
        |\    |
        | \   |_____
        KEVIN CROWLEY     
    \x00""")

if __name__ == "__main__": 
    argument=0
    print(get_banner())  
    print(list_of_options(argument))